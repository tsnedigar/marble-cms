const apiRoute = require('./api');
const adminRoute = require('./admin');

module.exports = {
    api: apiRoute,
    admin: adminRoute
};
