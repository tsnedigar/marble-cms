const express = require('express');
const route = express.Router({});

route.get('*', (req, res) => {
    console.log(req.path, req.query);
    res.sendStatus(200);
});

module.exports = route;
