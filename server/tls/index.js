const https = require('https');
const greenlock = require('greenlock-express');
const fsChallenge = require('le-challenge-fs');
const challengeStore = require('le-store-certbot');
const utils = require('../utils');

/**
 * @param {express} app
 * @param {string} email
 * @param {string} acmeDir
 * @param {Boolean} [staging]
 * @return {https.Server}
 */
const create = (app, email, acmeDir, staging) => {
    if (!email) {
        console.error('Email address is required for TLS');
        process.exit(1);
    }
    const le = greenlock.create({
        server: staging ? 'staging' : 'https://acme-v01.api.letsencrypt.org/directory',
        challenges: { 'http-01': fsChallenge.create({ webrootPath: acmeDir }) },
        store: challengeStore.create({ webrootPath: acmeDir }),
        approveDomains: utils.genApproveDomains(email)
    });

    return https.createServer(le.httpsOptions, le.middleware(app));
};

module.exports = { create };
