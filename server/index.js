const commander = require('commander');
const express = require('express');

const routes = require('./routes');
const utils = require('./utils');
const pkg = require('./package.json');

commander
    .version(pkg.version, '-v, --version')
    .option('-p, --port <port>', 'HTTP(s) binding port', utils.portChecker, 8080)
    .option('-a, --address <address>', 'IPv4 HTTP(s) binding address', utils.ipChecker, 'localhost')
    .option('-w, --web <dir>', 'Location of statically served files', null, './client')
    .option('-t, --tls', 'Use tls (letsencrypt)')
    .option('-e, --email', 'Email for letsencrypt (required for TLS)', utils.emailRegex)
    .option('-s, --staging', 'Use staging server for letsencrypt')
    .option('-c, --challenge-dir <dir>', 'Directory for ACME challenges', null, '/tmp/acme-challenges')
    .parse(process.argv);

const app = express();
app.use(express.json());
app.use('/api', routes.api);
app.use('/admin', routes.admin);
app.use('/portal', express.static(commander.web));

if (commander.tls) {
    const tls = require('tls');
    const server = tls.create(app, commander.email, commander.challengeDir, commander.staging);
    server.listen(443, () => {
        console.log('Listening on 443');
        console.log('Listening for ACME http-01 challenges on', this.address());
    });
} else {
    app.listen(commander.port, commander.address, () => {
        console.log(`Listening on ${commander.address}:${commander.port}`);
    });
}