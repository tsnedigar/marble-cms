const ipRegex = /^((?:[0-9]{1,3}\.){3}[0-9]{1,3}|localhost)$/;

module.exports = {
    portChecker: portStr => {
        const port = parseInt(portStr);
        if (port.isNaN(port)) {
            console.error(portStr, 'is not a number');
            process.exit(1);
        } else if (port < 1024 && ![80, 443].includes(port)) {
            console.error(port, 'is not a valid port number');
            process.exit(1);
        }
        return port;
    },
    ipRegex,
    ipChecker: ipStr => {
        if (!ipRegex.test(ipStr)) {
            console.error(ipStr, 'is not a valid IP (or \'localhost\')');
            process.exit(1);
        }
        return ipStr;
    },
    emailRegex: /^(.+@.+\..+)$/,
    genApproveDomains: email => (opts, certs, cb) => {
        if (certs) {
            opts.domains = certs.altnames;
        } else {
            opts.email = email;
            opts.agreeTos = true;
        }
        cb(null, { options: opts, certs: certs });
    }
};
 